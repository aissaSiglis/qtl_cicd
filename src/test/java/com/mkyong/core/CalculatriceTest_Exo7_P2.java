package com.mkyong.core;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class CalculatriceTest_Exo7_P2 {
	
	public class CalculatriceParameterizedTest {
	    @ParameterizedTest
	    @CsvFileSource(resources = "/test_data.csv")
	    public void testAddition(int a, int b, int expected) {
	        assertEquals(expected, Calculatrice.addition(a, b));
	    }
	}
}
