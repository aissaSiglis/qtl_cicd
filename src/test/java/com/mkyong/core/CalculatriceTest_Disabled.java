package com.mkyong.core;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import org.junit.Test;
import org.junit.AfterClass;
import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Before;
import java.util.concurrent.TimeUnit;
import org.junit.jupiter.api.Disabled;

public class CalculatriceTest_Disabled {
	
	private static long startTime;
    private static long totalDuration;
	
	
    @Test
    public void testAddition() {
        assertEquals(5, Calculatrice.addition(2, 3));
        assertEquals(-1, Calculatrice.addition(2, -3));
        assertEquals(0, Calculatrice.addition(0, 0));
    }

    @Disabled("Test désactivé temporairement")
    @Test
    public void testSoustraction() {
        assertEquals(2, Calculatrice.soustraction(5, 3));
        assertEquals(8, Calculatrice.soustraction(5, -3));
        assertEquals(0, Calculatrice.soustraction(0, 0));
    }

    @Test
    public void testMultiplication() {
        assertEquals(6, Calculatrice.multiplication(2, 3));
        assertEquals(-6, Calculatrice.multiplication(2, -3));
        assertEquals(0, Calculatrice.multiplication(0, 0));
    }

    @Test
    public void testDivision() {
        assertEquals(2, Calculatrice.division(6, 3));
        assertEquals(-2, Calculatrice.division(6, -3));
        assertEquals(0, Calculatrice.division(0, 5));
    }
    
    @Test(expected = ArithmeticException.class)
    public void testDivisionParZero() {
        Calculatrice.division(5, 0);
        fail("La division par zéro n'a pas levé d'exception");
    }
    
    
    @Test
    public void testFactoriel() {
        assertEquals(1, Calculatrice.factoriel(0));
        assertEquals(1, Calculatrice.factoriel(1));
        assertEquals(2, Calculatrice.factoriel(2));
        assertEquals(6, Calculatrice.factoriel(3));
        assertEquals(24, Calculatrice.factoriel(4));
    }

   
    @Test(expected = IllegalArgumentException.class)
    public void testFactorielNegative() {
        Calculatrice.factoriel(-1);
        fail("Devrait lancer une exception pour un nombre négatif");
    }
    
    @Test
    public void testCalculFactoriel() {
        assertEquals(120, Calculatrice.calcul(5, 0, "!")); // 5!
        assertEquals(1, Calculatrice.calcul(0, 0, "!")); // 0!
    }


    @Test
    public void testCalcul() {
        assertEquals(5, Calculatrice.calcul(2, 3, "+"));
        assertEquals(-1, Calculatrice.calcul(2, 3, "-"));
        assertEquals(6, Calculatrice.calcul(2, 3, "*"));
        assertEquals(2, Calculatrice.calcul(6, 3, "/"));
        assertEquals(0, Calculatrice.calcul(2, 3, "x"));
    }
    
    @BeforeClass
    public static void setUpBeforeAllTests() {
        System.out.println("Début de tous les tests pour la classe Calculatrice");
        startTime = System.nanoTime();
    }

    @AfterClass
    public static void tearDownAfterAllTests() {
    	long endTime = System.nanoTime();
        totalDuration = endTime - startTime;
        System.out.println("Fin de tous les tests. Temps total écoulé : " + convertDuration (totalDuration));
        System.out.println("Fin de tous les tests pour la classe Calculatrice");
    }


	@Before
    public void setUpBeforeEachTest() {
        System.out.println("Début d'un test");
    }

    @After
    public void tearDownAfterEachTest() {
        System.out.println("Fin d'un test");
    }
    
    private static String convertDuration(long duration) {
        long millis = TimeUnit.NANOSECONDS.toMillis(duration);
        long seconds = TimeUnit.MILLISECONDS.toSeconds(millis);
        long minutes = TimeUnit.SECONDS.toMinutes(seconds);
        return String.format("%d min, %d sec, %d ms", minutes, seconds % 60, millis % 1000);
    }  
}
