package com.mkyong.core;

import static org.junit.Assert.assertEquals;
import org.junit.Test;
import org.junit.AfterClass;
import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Before;

public class PerroquetTest {

    @Test
    public void testPerroquetParlant() {
        assertEquals("Bonjour", perroquet.perroquetParlant("Bonjour"));
        assertEquals("Hello", perroquet.perroquetParlant("Hello"));
    }

    @Test
    public void testPerroquetBonjour() {
        assertEquals("Bonjour Alice", perroquet.perroquetBonjour("Alice"));
        assertEquals("Bonjour Bob", perroquet.perroquetBonjour("Bob"));
    }
    
    @BeforeClass
    public static void setUpBeforeAllTests() {
        System.out.println("Début de tous les tests pour la classe Perroquet");
    }

    @AfterClass
    public static void tearDownAfterAllTests() {
        System.out.println("Fin de tous les tests pour la classe Perroquet");
    }

    @Before
    public void setUpBeforeEachTest() {
        System.out.println("Début d'un test");
    }

    @After
    public void tearDownAfterEachTest() {
        System.out.println("Fin d'un test");
    }

    
    
}