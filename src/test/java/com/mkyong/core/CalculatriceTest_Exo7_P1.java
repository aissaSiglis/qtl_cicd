package com.mkyong.core;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class CalculatriceTest_Exo7_P1 {

	public class CalculatriceParameterizedTest {

	    @ParameterizedTest
	    @CsvSource({
	            "2, 3, 5",
	            "-2, 3, 1",
	            "0, 0, 0",
	            "10, -5, 5"
	    })
	    public void testAddition(int a, int b, int expected) {
	        assertEquals(expected, Calculatrice.addition(a, b));
	    }
	}

}
